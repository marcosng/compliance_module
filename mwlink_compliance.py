import openpyxl
import ipaddress
from const import *
from func import get_frecuencia, get_potencia, get_radio_bw, log_message

###################LEO LA PLANILLA Y VALIDO LAS FILAS CON STATUS ACTIVOS##################
wb = openpyxl.load_workbook("report3.xlsx")
sheet = wb.active


for p in range(sheet.max_row):
    # OPCION1
    ############
    # cell_status = sheet.cell(row=p + 2, column=COLUMNA_STATUS).value
    # if str(cell_status).lower() in ['active', 'activo']:
    #     # Operar si el estado del enlace es activo
    #
    # else:
    #     # Operar si el estado del enlace no es activo (vacio, numero, etc)

    if str(sheet.cell(row=p + 2, column=COLUMNA_STATUS).value).lower() in ['active', 'activo']:
        #####################LEO LAS IPs#############################################################
        for col_ip in [COLUMNA_IP_A, COLUMNA_IP_B]:
            if col_ip == COLUMNA_IP_A:
                col_ftx = COLUMNA_FTx_A  # Asigno a col_ftx la frecuencia sitio A de la planilla sayri leida
                col_status_frec = COLUMNA_STATUS_FREC_A  # Asigno a col_status el valor de la columna donde voy a escribir el valor estatus
                col_valor_frec_radio = COLUMNA_VALOR_FREC_RADIO_A  # Asigno a valor_frec el valor de la columna donde voy a escribr el valor del radio

                col_pot = COLUMNA_RF_POWER
                col_status_pot = COLUMNA_STATUS_POTENCIA_A
                col_valor_pot_radio = COLUMNA_VALOR_POTENCIA_RADIO_A

                col_bandwith = COLUMNA_BW
                col_status_bandwith = COLUMNA_STATUS_BANDWITH_A
                col_valor_bandwith = COLUMNA_VALOR_BANDWITH_RADIO_A

            elif col_ip == COLUMNA_IP_B:
                col_ftx = COLUMNA_FTx_B
                col_status_frec = COLUMNA_STATUS_FREC_B
                col_valor_frec_radio = COLUMNA_VALOR_FREC_RADIO_B
                col_pot = COLUMNA_RF_POWER
                col_status_pot = COLUMNA_STATUS_POTENCIA_B
                col_valor_pot_radio = COLUMNA_VALOR_POTENCIA_RADIO_B

                col_bandwith = COLUMNA_BW
                col_status_bandwith = COLUMNA_STATUS_BANDWITH_B
                col_valor_bandwith = COLUMNA_VALOR_BANDWITH_RADIO_B


            # Lectura de dirección IP
            ip = sheet.cell(row=p + 2, column=col_ip).value

            # Chequeo de IP válida
            try:
                ipaddress.ip_address(ip)
            except ValueError:
                log_message(LOG_FILE, "Dirección IP inválida en fila " + str(p))
                print("Dirección IP inválida en fila " + str(p))
                continue


            #####################LEO DATOs FRECUENCIA,POTENCIA Y BANDWITH DEL SAYRI#######################################
            frec_sayri = str(sheet.cell(row=p + 2, column=col_ftx).value).split(';')
            potencia_sayri = str(sheet.cell(row=p + 2, column=col_pot).value).split(';')
            bandwith_sayri = str(sheet.cell(row=p + 2, column=col_bandwith).value).split(';')


            #########################Llamada a la funcion get_frecuencia(ip) y comparo#################################
            frec_radio = get_frecuencia(ip)
            if frec_radio is not None:
                for i in frec_sayri:
                    if float(i) in list(set(frec_radio)):
                        sheet.cell(row=p + 2, column=col_status_frec).value = 'ok'
                    else:
                        sheet.cell(row=p + 2, column=col_status_frec).value = 'not ok'

                # Escribo valores de frecuencia en Excel
                sheet.cell(row=p + 2, column=col_valor_frec_radio).value = ";".join(str(m) for m in frec_radio)


            #########################Llamada a la funcion get_potencia(ip) y comparo#################################
            potencia_radio = get_potencia(ip)
            potencia_sayri_2 = []
            for i in potencia_sayri:
                potencia_sayri_2.append(float(i))
            if potencia_sayri_2 == list(set((potencia_radio))):
                sheet.cell(row=p + 2, column=col_status_pot).value = 'ok'
            else:
                sheet.cell(row=p + 2, column=col_status_pot).value = 'not ok'

            # Escribo valores de potencia en Excel
            sheet.cell(row=p + 2, column=col_valor_pot_radio).value = ";".join(str(m) for m in potencia_radio)


            # ######################Llamada a la funcion get_radio_bw(ip) y comparo####################################
            bandwith_radio = get_radio_bw(ip)
            bandwith_sayri_2 = []
            for i in bandwith_sayri:
                bandwith_sayri_2.append(float(i))
            if bandwith_radio is not None:
                if list(set(bandwith_sayri_2)) == list(set(bandwith_radio)):  # comparo si las listas son iguales
                    sheet.cell(row=p + 2, column=col_status_bandwith).value = 'ok'
                else:
                    sheet.cell(row=p + 2, column=col_status_bandwith).value = 'not ok'

                # Escribo valores de ancho de banda en Excel
                sheet.cell(row=p + 2, column=col_valor_bandwith).value = ";".join(str(m) for m in bandwith_radio)



titles = ['Status_Frecuencia_Site_A', 'Valor_Frecuencia_Radio_Site_A', 'Status_Potencia_Site_A',
          'Valor_Potencia_Radio_Site_A', 'Status Bandwith_A', 'Valor_Bandwtith_radio_Site_A',
          'Status_Frecuencia_Site_B', 'Valor_Frecuencia_Radio_Site_B', 'Status_Potencia_Site_B',
          'Valor_Potencia_Radio_Site_B', 'Status Bandwith_B', 'Valor_Bandwtith_radio_Site_B']
offset = sheet.max_column

for j, title in enumerate(titles):
    sheet.cell(row=1, column=offset + j - 11).value = title

wb.save('comparacion2.xlsx')

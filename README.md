## 'Compliance module' para enlaces de CeraOptimizer Backahaul

El repositorio contiene el script necesario para realizar la comparación de frecuencia, potencia y ancho de banda entre lo cargada en el sistema y la configurada en los equipos.

1. Contar con un entorno virtual de Python 3.6.
2. Activar el entorno virtual.
3. Instalar las librerias necesarias: __pip install -r requirements.txt__
4. Conectarse a la VPN de Claro.
5. Desde CeraOptimizer Backhaul descargar _MwLink Report_ y renombrarlo a _report3.xlsx_.
6. Ejecutar el script _mwlink_compliance.py_.
7. Se generarán 2 archivos de salida:
* log.txt: contiene los errores ocurridos al intentar extraer la información de los equipos.
* comparación2.xlsx: copia del archivo de entrada con columnas extras.
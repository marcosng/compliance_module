# Archivo de funciones
import datetime
from const import LOG_FILE
from pysnmp.hlapi import *


def snmp_get(ip_address, oid, snmp_ver=0, community='public'):
    """

    Sends a SNMP v1 / 2c GET requests.

    :param ip_address: string. Device IP address.

    :param oid: string. Object Identifier

    :param snmp_ver: int. 0 for SNMPv1, 1 for SNMPv2c.

    :param community: string. Device community string

    :return:

    """

    g = getCmd(SnmpEngine(),

               CommunityData(community, mpModel=snmp_ver),

               UdpTransportTarget((ip_address, 161)),

               ContextData(),

               ObjectType(ObjectIdentity(oid))

               )

    errorIndication, errorStatus, errorIndex, varBinds = next(g)

    if errorIndication:

        return str(errorIndication)

    elif errorStatus:

        return '%s at %s' % (errorStatus.prettyPrint(), errorIndex)

    else:

        return varBinds[0]


def snmp_walk(ip_address, oid, snmp_ver=0, community='public', ):
    """
    Performs a SNMPWalk to a given IP address
    :param ip_address: string. IP address
    :param oid: string. OID
    :param snmp_ver: int. SNMP version 0=v1, 1=v2, 2=v3.
    :param community: string. Community string.
    :return:
    """
    output = []
    for (errorIndication,
         errorStatus,
         errorIndex,
         varBinds) in nextCmd(SnmpEngine(),
                              CommunityData(community, mpModel=snmp_ver),
                              UdpTransportTarget((ip_address, 161)),
                              ContextData(),
                              ObjectType(ObjectIdentity(oid)),
                              lexicographicMode=False):

        val = varBinds[0][1].prettyPrint()
        if val.isdigit():
            val = int(val)
        else:
            val = str(val)
        output.append(val)
    return output


def log_message(file, msg):
    f = open(file, 'a')
    time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    f.write(time + '\t' + str(msg) + '\n')
    f.close()


def get_radio_bw(ip):
    d = {13: 56000,
         15: 56000,
         16: 40000,
         18: 40000,
         22: 28000,
         23: 14000,
         33: 7000,
         1003: 56000,
         1004: 28000,
         1005: 28000,
         1006: 56000,
         1007: 40000,
         1008: 7000,
         1009: 14000,
         1010: 50000,
         1020: 10000,
         1021: 20000,
         1023: 3500,
         1025: 25000,
         1075: 1750,
         1203: 56000,
         1204: 28000,
         1205: 28000,
         1206: 56000,
         1207: 40000,
         1209: 14000,
         1210: 50000,
         1214: 28000,
         1217: 40000,
         1222: 25000,
         1501: 80000,
         1502: 56000,
         1503: 56000,
         1504: 28000,
         1505: 28000,
         1506: 56000,
         1507: 40000,
         1508: 7000,
         1509: 14000,
         1510: 50000,
         1520: 10000,
         1521: 20000,
         1523: 3500,
         1525: 25000,
         1527: 40000,
         1534: 28000,
         1537: 40000,
         1700: 125000,
         1801: 28000,
         1901: 28000,
         1902: 40000,
         1903: 56000,
         1904: 50000,
         1951: 28000,
         1952: 40000,
         1953: 56000,
         4501: 80000,
         4502: 56000,
         4504: 28000,
         4505: 28000,
         4506: 56000,
         4507: 40000,
         4509: 14000,
         4510: 50000,
         4511: 112000,
         4514: 28000,
         4517: 40000,
         4521: 20000,
         4525: 28000,
         4700: 125000,
         4701: 62500,
         4702: 250000,
         4704: 500000,
         4706: 50000,
         4707: 100000,
         4901: 28000,
         4902: 40000,
         4903: 56000,
         4905: 80000,
         4911: 112000}
    try:
        scripts_id = snmp_walk(ip, '1.3.6.1.4.1.2281.10.7.4.6.1.2')
        bw = []
        for script_id in scripts_id:
            bw.append(d[script_id] / 1000)
        return bw

    except:
        log_message(LOG_FILE, "Error al intentar recolectar ancho de banda de ip " + ip)
        print("Error al intentar recolectar ancho de banda de ip " + ip)


def get_frecuencia(ip):
    try:
        f = snmp_walk(ip, '1.3.6.1.4.1.2281.10.5.2.1.3')
        frecuencia = []
        for i in f:
            frecuencia.append(i / 1000)
        return frecuencia

    except:
        log_message(LOG_FILE, "Error al intentar recolectar frecuencia de ip " + ip)
        print("Error al intentar recolectar frecuencia de ip " + ip)


def get_potencia(ip):
    try:
        p = snmp_walk(ip, '1.3.6.1.4.1.2281.10.5.1.1.27')
        potencia = []
        for i in p:
            potencia.append(i)
        return potencia

    except:
        log_message(LOG_FILE, "Error al intentar recolectar potencia de ip " + ip)
        print("Error al intentar recolectar potencia de ip " + ip)



